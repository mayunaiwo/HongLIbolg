
module.exports = {
    title: 'Hong Li',
    description: 'HongLi的个人bolg,基于vuepress的一个静态框架去写的',
    // base:'/myvuepress/', //githubpage
	'@vuepress/pwa':{
					  serviceWorker: true,
					    updatePopup:{
							message:'发现新内容可用',
							buttonText:'刷新',
						}
	},	
    plugins: [
      '@vuepress/back-to-top',
      'vuepress-plugin-nprogress'
    ],
    head: [
      ['link', { rel: 'icon', href: '/index.ico' }],
      ['link', { rel: 'manifest', href: '/maninfest.json' }],
      ['link', { rel: 'stylesheet', href: '/css/style.css' }]
    ],
    //是否显示行号
    markdown: {
      lineNumbers: true
   },
      themeConfig: {
      displayAllHeaders: true,  //页面的标题链接
      lastUpdated: '最后更新',  //最后更新你只需要给一个字符串
      editLinks:true,
      search: true, //内置搜索
      searchMaxSuggestions: 10,
      editLinkText:'编辑文档!',
      docsDir:'docs',
        nav: [
          { text: '首页', link: '/' },
          // { text: '关于我', link: '/about1/' },
          {
            text: '关于我',
            items: [
              { text: '关于我', link: '/about/' },
              { text: '问题', link: '/problem/' },
            ]
          },
          {
            text:'大前端',
            items:[
             {
            text :'基础',
              items: [
                 { text: 'Html', link: '/web/' },
                 { text: 'CSS3', link: '/web/CSS3' },
				  { text: 'Flex布局', link: '/web/flex' },
              ]
             },
             {
               text:'进阶',
               items:[  
                { text: 'Vue文档', link:  '/vuenotebock/' },
                { text: 'js学习', link: '/js/' },
				 { text: 'AjaX学习', link: '/js/AjaX' },
				  { text: 'MarkDown语法详情', link: '/js/markdownYUFA' }
               ]
             }
            ],  
          },
          {
          text:'简历',
          items:[
            {
              text:'简历',
              items:[
                { text: 'Resume', link: '/Resume/' },
              ]
            },
            {
              text:'友链',
              items:[
                { text: '友情链接', link: '/style/' }
              ]
            }
           ]
          },
              // { text: 'Resume', link: '/Resume/' },
          // { text: '学习', link: '/style/' },
       
          {
            text: 'Languages',
            items: [
              { text: 'zh-CN', link: '/' },
              { text: 'en-US', link: '/en/' }
            ]
          },
          {text:'vuepress文档',link:'https://vuepress.vuejs.org/zh'},
          { text: 'github', link: 'https://github.com/ZhongHongLi' },
          {text:'weibo',link:'https://weibo.com/u/3314319737?is_all=1'},
        ],
        sidebar: {
           '/about/':[
             '',
           ],
            '/web/':[
                '', //获取所有
				'CSS3',
				'Flex'
            ],
            '/vuenotebock/':[
              '',
            ],
            '/style/':[
              '',
            ],
            '/opp/':[
              'oop',
            ],
			 '/problem/':[
			  '',
      ],
      '/js/':[
        '',
		'AjaX',
		'markdownYUFA'
      ]
        },
        sidebarDepth: 2,// 标题深度  
        displayAllHeaders: true , //显示标题链接
        activeHeaderLinks: true, //true   标题刷新
      },
  '@vuepress/back-to-top': true,
  }