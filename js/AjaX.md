### 这是一个ajax的页面
 
 `什么是ajaxa?`
 > *  异步的javascriptXMl不用通过刷新可以获取数据,官方一点就是`Asynchronous javascript and XML （异步的javascript 和XML）`.
 
 ```js{2}
   	//创建ajax对象
			//xhr.readyState
			//0.创建了ajax对象还没有使用open方法和设置请求方式和地址
			//1.使用open方法设置请求方式和地址,
			//2.send方法发送请求
			//3.正在接受收据
			//4.完全接受收据后
			
			//xhr.setRequestHeader 设置请求头信息 ('name','value')设置请求头信息
			//name请求头设置项的名称,value请求头设置项的值 主要post请求 
			//xhr.onreadystatechang 回调readyState
			//xhr.status请求状态值200
			//xhr.responseText 接受返回的数据,普通文本字符串格式
	       document.getElementById('btn').onclick=function(){
		
			    let xhr=new XMLHttpRequest();
			      xhr.onreadystatechange=function(){
			   			 if(xhr.readyState==4){
			             document.querySelector('div').innerText=xhr.responseText;
						 console.log(xhr.responseText)
			   			 }
			   }
			   xhr.open('GET','01.test',true);
			   xhr.send();		
		   }
 ```
 
 ~~删除线~~
[今日学习](http://dcloud.io)


