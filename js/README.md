### 初始javascript

 `ECMAscript  DOM  BOM 三部分组成`
 
 :100:
 概述:变量是程序在内存中申请的一块用来存放数据的空间  var 变量名 值;
 变量的使用:声明 赋值
 ```js{1}
    //var 声明变量
    var age //声明一个变量
    //赋值
    var age=18
    声明并赋值叫变量的初始化
   
   变量语法扩展 

   更新变量  声明多个变量  只声明未赋值是undefind

   不声明不赋值 直接使用会报错   add is not defined  at <anonymous>:1:13

   不声明直接赋值使用 

   age =10   直接赋值是可以用 但是变为全局变量  不提倡是使用
   变量声明的规范 区分大小写  不能用关键字保留字  var  for   座位变量名
   尽量不要使用name使用变量名
 ```



 ### 变量的总结 :100:
![An image](../.vuepress/public/bianliang.png)

**五种数据类型 :
Number, String, Boolean,Undefined,Null ,Function,Object**

Js是动态语言,变量的数据类型是可以变化的  var x=10; x="pink"

``` js{1}
   数字型 Number  整数  小数    
   //1.八进制  0~7我们程序里面数字前面加0 便是八进制
   var num1=010;
  
  //2.十六进制 0~9 a~f  
   var  mum=0xa;
  在js中八进制前面加0,十六进制前面加0x
  javascript数字型中的最大值最小值
  最大值console.log(Number.MAX_VALUE);
  最小值console.log(Number.MIN_VALUE);
  无穷大:Infinity
  无穷小:-Infinity;
  非数值NaN 

  isNaN 用来判断一个变量是否为非数字的类型,返回true,或者false;

  String     
  获取字符的长度length 
  字符串拼接   

  Boolean  true  false    
  布尔中两个值响家 true 当1来看 false 当0来看

  Undefind 未定义   声明未赋值就是未定义   undefined和数字相加 最后结果是NaN 是一个非数值
  null 空

  获取数据类型typeof
  
  数据类型的转换 

  转换成字符串型: toString()  String()强制转换  字符串拼接-隐式转换;
  
  var num=10;
  var str=num.toString();
  console.log(str);

  console.log(String());

  console.log(num+'');

  转换成数字型
  parseInt();  得到的是整数 只取整数

  parseFloat() 得到是小数  浮动数

  Number()  

  利用了算数  - * / 隐式转换 

  转换成布尔型
  Boolean();
```


### 数组  :100:

 **数组的概念:Array :一组数据的结合,其中每个数据被称为元素,在数组中任意存放任意类型的元素,数组是一种将一组
 数据存储在单个变量名下的优雅方式**

``` js{2}
 利用new关键字创建数组
  var arr=new Array();  //创建一个新的空数组

 利用字面量创建数组 最常用的
 var  数组名 =[];   var arr=[]; 
 
 获取数组的元素   
 索引
 var arr=[1,5,9,6,5,'pink'];
 console.log(arr[0]); //1

 遍历数组  循环
 for(var i=0; i<arr.length; i++){  //i 计数器
    console.log(arr[i]);
 }
 
 删选数组中的最大的值并打印出来

 let arr=[1,10,60,70,70,55,3,10,20];
 let newArr=[]; 
 let j=0;
 for(var i=0; i<arr.length; i++){
   if(arr[i]>=10){
     arr[j]=arr[i];
     j++;
   }
 }
  console.log(newArr);

 冒泡排序
 var arr=[1,2,3,4,5];
 for(var i=0; i<arr.length-1;i++){ //外层循环趟数
   for(var j=0; j<arr.length-i-1; j++){ //内层每一趟交换的次数
      var temp=arr[j];
      arr[j]=arr[j+1];
      arr[j+1]=temp;
   }
 }
 console.log(arr);

```

### 函数

 **函数的概念:函数就是封装了一段可以重复执行调用的代码块，让大量代码重复使用**

 ```js{}
  函数的使用:先声明函数  再调用
//1.声明函数
  function  函数名(){
    //函数体
  }

//调用函数   函数名();


//2.我们可以利用函数的参数实现函数重复不同的代码
 function  函数名(形参1,形参2...){

 }

 函数名(实参1,实参2...);
 ```


:tada: :100: :bamboo: :gift_heart: :fire:


### 简单数据类型与复杂数据类型
 

``` js {2}

 - 值类型:简单数据类型/基本数据类型,在存储是变量中存储的事值本身，因此叫做值类型 
 string ,number,boolean,undefined ,null
 引用类型:复杂数据类型，在存储时变量中存储的仅仅是地址,因此叫做引用数据类型
 通过new关键字创建的对象,object,Array,Date

 简单数据类型,是存放在栈里面 里面直接开辟一个空间存放的值
 复杂数据类型 首先在栈咯面存放地址  十六进制表示 然后这个地址指向堆里面的数据

 简单类型传参
 ```

 ### 面向对象

 - 面向过程:一步一步 前因后果
 - 面向对象:只求结果 不管实现过程,实际上就是对面向过程中的每一步进行封装
 
 >三大特性   :100:
 - 封装:将面向过程每一步进行推进:对同种对象的属性和方法进行抽象，成为一个类(js中没有类的概念，实际上也是一个对象)，然后通过类的方法和属性来访问类
 - 继承:在封装的基础上，将同类事物继续抽象，继承时，子类拥有父类的属性和方法，同时拥有自己特有的属性和方法.
 - 多态:不同对象对同一事物做出不同的回应，通常出现在继承后对方法的重写.
 - 封装 继承 多态都是对具体事务的抽象.
 - 抽象是实现面向对象编程的基本思想.
 - 抽象抽取事务主要特征对事务进行描述.


### 构造函数

 - 构造函数和原型
   >构造函数是一种特殊的函数，主要用来初始化对象,即为对象成员变量赋初始值
 
 - 对象字面量

 - var obj={}

 - new 对象

 - var obj=new Obeject();
 - 构造函数
   function  Star(name,age){
	   this.name=name;
	   this.age=age;
	   this.sing=function(){
		   console.log('我会唱歌');
	   }
   }
  var ldh=new Star('liudehua',18);
  var zxy=new Star('zhangxueyou',19);
  ldh.sing();
  
  原型prototype   构造函数通过原型分配的函数是所有对象所共享的 
  原型是一个对象
  原型作用  我们公共的方法放到原型对象身上，不会开辟新的内存空间
  我们的公共属性一般放到构造函数里面  
  构造函数.prototype.sing=function(){
	  
  }
  
  - 对象原型__proto__
    对象都会有一个属性__proto__指向构造函数的prototype原型对象，之所以我们可以使用构造函数prototype原型对象的属性和方法
  ，就是因为对象有__proto__原型的存在
  
   对象原型(__proto__)和构造函数(prototype)原型里面都有一个属性constructor属性，constructor属性我们称为构造函数,因为它指回构造函数本身
  
   - constructor主要用于记录该对象引用于那个构造函数,它可以让对象重新指向原来的构造函数
     修改了原来的原型对象，给原型对象赋值的是一个对象,则必须手动调价constructor这个属性指回原来的构造函数
  
  ### 构造函数,实例对象,原型对象三者之间的关系
  ![An image](../.vuepress/public/gouzao.png)
  
  
  ### 原型链 
  ![An image](../.vuepress/public/lians.png)
  
-  原型查找原则机制:
1.当访问一个对象的属性(包括方法)时，首先查找这个对象自身有么该属性
2.如果没有就查找它的原型__proto__指向prototype原型对象.
3.如果还没有就查找原型对象的原型(Obeject的原型对象).
4.依次类推一直找到Obeject为止(null)
5.__proto__对象原型的意义就在于为对象成员查找机制提供一个方向，或者说一条路线
	  
### 原型对象this指向问题
  
 构造函数中，里面this指向是指的new对象实例
 原型对象函数里面的this指向的事实例对象
 
 内置对象
 通过内置对象我们可以对原型对象扩展内置对象方法
 数组和字符串内置对象不能给原型对象覆盖操作 Array.prototype={} 这样写会报错。只能是Array.prototype.xxx=function(){}的方式
 
 ``` js{3}
 
     Array.prototype.sum=function(){
		 let sum= 0 //初始化为0
		 for(let i=0; i<this.length; i++ ){
			 sum+=this[i];
		 }
		 return sum;
	 }
	 var arr=[1,10,50];
	 console.log(arr.sum())//调用  61
	 console.log(Array.prototype); //你在控制台会看到原型对象里面已经有这个求和sum的方法 
	 
	不能跟上面说的以对象的方式 来使用会报错
 ```
   
  - 继承 
  ES6之前并没有给我提供extends继承,我们采用的`构造函数+原型对象`模拟实现继承，被称为`组合继承`.
  
  call()调用这个函数,并且修改函数运行时的this指向.
  `fun.call(thisArg,arg1,....)`
  
  - thisArg:当前调用函数this的指向对象
  
  - arg1,arg2:传递的其他参数
   call方法可以调用函数，也可以改变还函数的this指向,此时这个函数的this就指向的你调用的函数的这个对象
   
  - 借用构造函数继承父类属性
   核心原理:通过call()把父类型的this指向子类型的this,这样就可以实现子类型继承父类型的属性  
 ``` js{3}
     //借用父构造函数继承属性
	 //1.父构造函数
	 function Father(name,age){
		 //this指向构造父构造函数的对象实例
		 this.name=name;
		 this.age=age;
	 }
	 //2.子构造函数
	 function Son(name,age,score){
		 //this指向子构造函数额对象实例
		 Father.call(this,name,age);
		 this.score=score;
	 }
   var son =new Son('ldh',20);
   console.log(son);

 ```
 
 ### 借用构造函数继承父类方法
  ``` js{3}
     //借用父构造函数继承属性
 	 //1.父构造函数
 	 function Father(name,age){
 		 //this指向构造父构造函数的对象实例
 		 this.name=name;
 		 this.age=age;
 	 }
	 Father.prototype.sing=function(){
		 console.log('我会唱歌');
	 };
 	 //2.子构造函数
 	 function Son(name,age,score){
 		 //this指向子构造函数额对象实例
 		 Father.call(this,name,age);
 		 this.score=score;
 	 }
	 Son.prototype=Father.prototype;
	 
   var son =new Son('ldh',20);
   console.log(son);
 
 ```
 

