---
sidebar: false
---

::: tip
 ### 个人简历
:::

个人信息

姓名：张洪利 

性别：男

出生日期：1995年9月9日

现居城市：武汉

年龄：23

邮箱：zhanghlwangyi@126.com

联系电话：17671644416

婚姻状况：未婚

国籍：中国

政治面貌：团员

::: warning
综合技能
:::

1.精通HTML和CSS，熟练使用DIV+CSS进行网页布局和设计。

2.熟悉HTML5,CSS3,javascript,jquery,Bootstrap等前沿前端技术。

3.熟练使用git,gulp,webpack,less等工具的使用

4.了解原型链,闭包,面向对象,MVC/MVVM架构思想

5.了解前端自动化测试实现或集成工具的使用等

6.熟练VUE,生命周期,aiox,vue-x,组件化开发

7.了解前端项目模块化解决方案，对高并发、大访问量时前端各种场景的处理有一定的心得

::: danger
个人评价
:::

1.喜欢钻研新技术,逛论坛,bolg,

2.对待工作认真,积极面对工作中遇到的问题,克服工作中的障碍

3.如何看待加班,偶然是可以的,

4.规划好了自己三年的目标,技能,生活等


## 个人信息

| key      | value                                                     |
| -------- | --------------------------------------------------------- |
| nickname | 洪利/HongLi                                               |
| birthday | 1995.09.29                                                |
| tel/QQ   |  17671644416/ 948605929                                   |
| eml| [@邮箱](https://www.zhanghlwangyi@126.com)  [@github](https://github.com/ZhongHongLi)|



