---
home: true
heroImage: /logopic.gif
tagline: HongLi s personal bolg, based on a static framework of vuepress, the front of the rookie.
actionText: HongLi bolg →
actionLink: "/about/"
features:
  - title: HTML
    details: Skilled html semantic tag, html5 new features, good at page layout and browser compatibility IE10.
  - title: CSS
    details: Skilled in CSS3 new features, Bootstrap, ElmentUl, Canvas.
  - title: JS
    details: Native JS doesn't have to say too much, too much to learn.
footer: MIT Licensed | Copyright © 2019-present Hong Li
comment: false
single: true
---
