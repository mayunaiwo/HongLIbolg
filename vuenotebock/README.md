##  vue的笔记

 `如何快速上手vue,了解vue之前你必须会javascript和了解Es6语法`



###  安装vuecli脚手架
 
 ``` js{2}
   全局安装vuecli
   npm install  -g  vue-cli

   cd到项目文件
   vue init webpack

   初始化完成
   cnpm install 

   最后运行 /打包
   npm run dev   / npm run build

 ```

---
目录结构
---

 ![An image](../.vuepress/public/product.png)


## ES6

``` js{1,3}
 1.ECMAScript和javascript   ECMA是标准，js是实现  ECMAscript≈JS   ES6
 ECMA6.0  
 es6 chrome /firefix 移动端， NodeJS
 提前编译
 在线编译
         
       1.变量
       2.函数
       3.数组
       4.字符串
       5.面向对象
       6.Promise
       7.generator
       8.模块化
       
        var  
        1.可以重复声明
        2.无法限制修改
        3.没有块级作用域
        {
             这个叫语法块
        }
       
        let      不能重复声明    变量-可以修改,块级作用域
        const    不能重复声明    变量- 不可以修改，块级作用域  
```


::: danger
 提示: MVC和MVVM
:::
` MVC 是后端的分层开发概念；
 MVVM是前端视图层的概念，主要关注于 视图层分离，也就是说：MVVM把前端的视图层，分为了 三部分 Model, View , VM ViewModel`

`VUE是一个MVVVM的框架`
----

![An image](../.vuepress/public/MVC.png)



###  Vue生命周期
----
![An image](../.vuepress/public/shenming.png)

`生命周期:`
```js {3}
    <div id="app">
      {{message}}
    </div>

    //1.了解vue的生命周期
       // 2.el 会在里面写   还可以后面写   vm.$mount('#app'); 
        let vm=new  Vue({
            el:"#app",
            data:{
                   message:'hello'
            },
            beforeCreate () {  //beforeCreate生命周期函数   //创建之前
                console.log(this.$el);  //undefined
                console.log(this.$data);  //undefined
            } ,
            created(){ //created 生命周期函数  创建
                console.log(this.$el);  //undefined
                console.log(this.$data);  // {__ob__: Observer}  可以获取到了
            },
            beforeMount () {  //beforeMount  //生命周期
                console.log(this.$el);  //可以获取到  {{message}}
                console.log(this.$data)  //可以获取到
            },
            mounted () { //mounted  //生命周期
                console.log(this.$el);  //可以获取到  {{hello}}
                console.log(this.$data) //可以获取到
            },
            beforeUpdate () {//  beforeUpdate //生命周期
                // console.log('更新前');      //数据更新 一下就会触发   更新前数据不会变  还是hello
                // consoleo.log(this.message);  //数据已经改变了   hello vue    vm.message="hello vue"
                alert(this.$el.innerHTML);   //渲染没有改变   还是  HELLO      
            },
            updated () {
                console.log('更新后')
                console.log(this.message); //数据变l
                console.log(this.$el.innerHTML); //渲染已经出来了
            },
            beforeDestroy () {   //beforeDestroy
                console.log('销毁前');
                // console.log(this.$data);   销毁了数据在,但是不可用
            },
            destroyed(){
                console.log('销毁后');
                // console.log(this.$data);
            } 
        })
       // vm.$mount('#app');       
```




###  Vue指令
 `v-text :渲染表达式`
 `v-html :会渲染html标签,要小心不要被xss攻击了`

 ```js {2}
 
    var vm = new Vue({
        el:'#app',
        data:{
            messages:'hello world'
        },
        methods:{
        }
    })

 ```


 ### computed计算属性与methods方法

 ``` js{3}
    //1.computed参数：计算属性  可以简化我们表达式操作，方便我们去看,我们直接在方法
    //可以替代一些负责的表达式操作(写法更加简单)
    //2.计算属性可以对数据进行缓存，一旦数据进行改变的时候才会重新渲染
    //数据不变的时候，会执行缓存中的内容
    //计算属性computed 比 methods更优化
    //变化的是，计算属性里的数据
    //一旦计算属性的数据发生变化，里面的数据也会发生变化  计算属性默认不能进行修改的
    //但是可以通过set(),get()的方法可以进行实现

 <div id="app">
        <!-- {{message}}
           {{newMessage}}
           {{message.split('').reverse().join('')}} -->
          计算： {{reverseMessage}}<br>
          方法： {{reversMessageFn()}}
</div>

    var vm=new Vue({
       el:"#app",
       data:{
           message:"尤雨溪",
       },
       methods:{
          reversMessageFn(){
            return this.message.split('').reverse().join('');
          }
       },
       computed:{ 
            newMessage(){
                 return 'hello youyuxi'
            },
            // reverseMessage(){
            //     return this.message.split('').reverse().join('');
            // }  //上面不可以进行设置
            reverseMessage:{
                set(newVal){ //可以进行设置
                    this.message=newVal;
                 },
                get(){ //获取
                    return this.message.split('').reverse().join('');
                }
            }       
        }
       })      
 ```


### 生命周期  
- beforeCreate创建之前
- created创建完成
- beforeMount 挂载之前
- mounted挂载之后   ajax
- beforeDestory组件销毁前调用
- destoryed组件销毁后调用

### 全局安装脚手架 :tada:
``` js{1}
npm install --global vue-cli /yarn add -D vue-cli
初始化
vue init webpack my-project
安装依赖
cd my-project
npm install /cnpm i
npm run dev
```

 ### javascript三种声明方式 var let const之ES6
 
  var 声明一个变量,可以初始化一个值
  let 声明一个块作用域的局部变量,可以初始化一个值
  const 声明一个块作用域,但是只是只读常量 const abc=2; 不能改变的量
  不能  const abc;  在赋值  abc=2; 会报错 常量必须在声明中赋值
  let和const的区别就是const一旦声明就不能改变
``` js{1}
  var abc=1;
  let abc=2;
  const abc=3;
  
  
  var 会进行变量提升
  let 不会变量提升 只会在块级作用域内有效  let变量不会被重复声明
  const 常量
  
  ES6之前的作用域
  全局作用域
  函数作用域
  eval作用域	
  
  块级作用域 {}  可以嵌套
```
 声明常量
   
 ```js{3}
   const abc=123;
   console.log(abc);
  常量声明过后不能被修改;
  
  常量为引用类型的时候可以修改为引用类型
  const xiaoming={
	  age:13,
	  name:'小明'
  };
  console.log(xiaoming);  //age:13 name:'小明'
  xiaoming.age=22;
  console.log(xiaoming); //age:22 name'小明'
 
  const arr=[];
  arr.push(1);
  console.log(arr);
  
  防止常量为引用类型被修改的情况
  Object.freeze()//冻结
  const xiaoming={
  	  age:13,
  	  name:'小明'
  };
  Object.freeze(xiaoming)
  console.log(xiaomimg);  
  xiaoming.age=22;
  console.log(xiaoming); 
  
  同是也不能给小明扩展方法
  
  Es6之前怎么声明常量的扩展
  1.假装是常量
  var BASE_COLOR='#FFF0000'
  
  Object.defineProperty();
  
  var CST={};
  Object.defineProperty(CST,'BASE_NAME',{
	  value:'xiaoming',
	  writable:false   // 定义不能修改
  });
 ```
 
 - 总结:
 let 不能被重复声明
 不存在变量提升
 let声明的变量只在当前(块级)作用域内有效
 暂存死区,块级作用域
 const
 不能重复声明
 一旦声明常量,就不能再改变(引用类型需要冻结) const跟let的不同之处
 let声明的变量只在当前(块级)作用域内有效
 ----
 ### ES6中的结构赋值
 -  结构赋值语法是一个javascript表达式，这使得可以将值从数组或属性对象提取到不同的变量中
``` js{3}
- 数组的结构赋值
   数组的结构赋值
  const arr=[1,2,3,4];
  let [a,b,c,d]=arr;
  
  复杂一点的匹配规则
  const arr=['a','b',['c','d',['e','f','g']]]
  const [ ,b]=arr;
  
  const [,,g]=['e','f','g'];
  
  const [,,[,,g]]=['c','d',['e','f','g']];
  
  
  扩展运算符
  const arr1=[1,2,3];
  const arr2=['a','b'];
  const arr3=['zz',1];
  const arr4=[...arr1,...arr2,...arr3]; //合并
  
  默认值
  const arr=[1,2,3,4];
  const [a,b,...c]=arr  c//[3,4]//扩展运算符要放到最后面
  
  交换变量
  let a=20;
  let b=10;
  
  let temp;
  temp=a;
  a=b;
  b=temp;
  //以前的写法
  [a,b]=[b,a];
   
  接收多个,函数返回值
   function getUserInfo(id){
	   //..ajax
	   return [
		   true,
		   {
			  name:'小明',
			   gender:'女',
			   id:id
		   },
		   '请求成功'
	   ];
   };
   const [status,data,msg]=getUserInfo(123)
  
```
- 对象的结构赋值
```js{3}
 对象的结构赋值与数组的结构赋值相似
 等号左右两边都为对象结构 const{a,b}={a:1,b:2}
 左边的{}中为需要赋值的变量
 右边为需要结构的对象
 
 用途:
 提取对象属性
 使用对象传入乱序的函数参数
 获取多个函数返回值
 
 const obj={
	 name:'小明',
	 age:23
 }
 
 const{name,age}=obj;
 
 const {name,hobby:[hobby1],hobby }={
	 name:'xiaogong',
	 hobby:['学习']
 }
- 字符串的结构赋值
- 数组与布尔值的结构辅值
- 函数参数的结构赋值


```


### ES6扩展

``` js{3}
模板字符串
 const xiaoMing={
	 name:'小明',
	 age:20,
	 say1:function(){
		 console.log('我叫'+this.name+',我今年'+this.age+'岁');
	 },
	 say2:function(){
		 console.log(`我叫${this.name},我今年岁${this.age}岁`)
	 }
 }
 xiaoMing.say1();
 xiaoMing.say2();
 
 部分新方法
 padStart padEnd 
 repeat
 startsWith  endsWith
 includes
 
 for of 遍历字符串
 
 for循环
 for(var i=0; let=str.length; i<len; i++){}
 转成数组后遍历
 var Ostr=Array.prototype.slice.call(str);
 
 var Ostr=str.split('');
 
 const Ostr =[...str];
 
 const [...str]=ste
 
 console.log(Ostr);
```
### 函数参数的默认值
``` js{1}
function  add(a,b=999){
	console.log(a,b);
}
add(1);

//箭头函数
const add1=(a,b)=>a+b;

const add2=function(a,b){
	return a+b;
}
console.log(add1(2,2));
console.log(add2(1,3 ))

数组的方法
Array.from  
Array.of
Array.fill
Array.includes

keys
values
entries
find
findeIndex
```

### ES6中Promise
```js{1}
Promise对象用于表示一个异步操作的最终状态(完成或失败)以及其返回的值
同步 一步一步的执行 alert for 
异步互不干扰 setTimeout 
使用promise(then,catch,finally)
Promise.all&Promise.race
Promise.resolve&Promise.rejcet

回调
function f(val){
	return new Promise((resolve,reject)=>{
		if(val){
			resolve();
		}else{
			reject('404');
		}
	});
}
f(false)
.then(()=>{
	console.log('成功了')
	},e=>{
		console.log(e);
	})

 ---- 
 - Promise的三种状态
 pending-进行中
 fulfilled-成功
 rejected-失败
 状态的改变不可以逆一旦决议就不能再修改
 
 pending=>  fulfilled成功
 pending=>   rejected 失败
 
 Promise.all方法可以把多个Promise实例包装成一个新的Promise实例
 Promise.all([promise1,promise2]):Promise
 Promise.race([Promise1,Promise2]):Promise
 
 ES6中的类class
 
 类与对象 OOP面向对象开发  
 class Car{
	 constructor(...arg){
		console.log(arg);
	 }
 }
 
new Car('蓝色',1000,'德国');
```


