 ###  js高级第一天
类的概念,在Es5中是没有类的概念的,ES6中新增的类的概念
``` js{1}
    //创建一个类class 定义一个类
	class Star{
		constructor(name,age){
	      this.name=name;
		  this.age=age;
			
	}
	sing(song){
		console.log(this.name+song);
	}
	}
	let ldh=new Star('刘德华',53);
	var zxy=new Star('张学友',54);
	console.log(ldh,zxy);
 	
     ldh.sing('冰雨');
	//1.通过class关键字创建类,类名我们还是习惯首字母大写
	//2.类里面有个constructor函数,可以接收传递过来的参数,同时返回实例对象
	//3.constructor函数只要new生成的实例时,就会自动调用这个函数,如果我们不写这个函数类也会自动生成这个函数
	//4.生成实例new不能省略
	//5.最后注意语法规范,创建类类名后面不要加小括号生成实例类名后面加小括号,构造函数不需要加function
```

 - 类中的继承
 ``` js{1}
   class Father  {
  			 constructor(x,y) {
  			     this.x=x;
  				 this.y=y;
  			 }
  			 sum(){
  				 console.log(this.x+this.y);
  			    }
  			 }
  			 class Son extends Father{
  				 constructor(x,y){
  					 super(x,y);  //调用了父类中的构造函数
  				 }
  			 }
   let son =new Son(1,2);
   let son1=new Son(11,22);
   son.sum();
   son1.sum();
 ```
 
 ### 函数的进阶
 - 函数的多种定义和调用方式
 - 函数内部的this指向问题
 - 严格模式的特点
 - 函数座位参数和返回值传递
 - 闭包的作用
 - 递归的两个条件
 - 深浅拷贝的区别

 ### 函数的定义的调用
```js{2}
  函数的定义的调用
  - 函数的定义
  1.function fn(){}命名函数
  2.var fun =function (){} //匿名函数
  
  3.new Function('参数1','参数2','函数体')
    var f=new Function();
	函数里面必须是字符串  //效率低
	console.dir  可以打印一个对象的所有属性和方法  函数也是对象
	
	-函数的调用
	-1.普通的函数
	function fn (){
		console.log('调用函数');
	}
	fn();
	
	-2.对象的方法
	 let o={
		 sayHi:function(){
		console.log('tiaoyong')	 		 
	}
	}
	o.sayHi();
	
	-3.构造函数
	function Star(){
		console.log('我是一个构造函数');
	}
	new Star();
	
	-4.绑定事件函数
	btn.onclick=function(){} //点击了按钮就调用
	
	5.定时器函数
	setInterval(function(){},1000)//1秒钟调用一次
	
	6.立即执行函数
	(function(){
		console.log('自己调用');
	})();  //立即执行函数是自己调用
```  

 ###  函数this指向问题
 ``` js{1}
    普通函数的this执行window
	对象的方法this指向这个对象
	构造函数this指向实例对象
	原型对象里面的this指向的也是这个实例
	function Star(){
		Star.protptype.sing=function(){
			
		}
	}
    var ldh=new Star()   this
	
	绑定事件函数 this指向的事函数调用者  ,
	
	定时器函数  this指向的函数window
	
	立即指向函数  this 指向也是window
	
 ```
 - 改变函数内部this指向
  javascript为我们专门提供了一些函数方法来帮我们更优雅的处理函数内部this的指向问题
  常用的有bind(),call(),apply()三种方法
  
  call方法
  call()方法调用一个对象,简单理解为调用函数的方法,但是它可以改变函数的this指向
  fun.call(thisArg,arg1,agr2,...)
``` js{1}
   var o={
	   name:'ady'
   }
   function fn(a,b){
	   console.log(this);
	   console.log(a+b);
   };
	fn.call(o,1,2)
	//call第一个可以调用函数 第二个可以改变函数内的this指向
	//call的主要作用可以实现继承
	
	--- 
	function Father(unmae,age,sex){
		this.unmae=uname;
		this.age=age;
		this.sex=sex;
	}
	function Son(uname,age,sex){
		Father.call(this,uname,age,sex);
	}
	var son =new Son('刘德华',18,'男');
	console.log(son);
	
	apply方法
	apply()方法调用一个函数,简单理解为调用函数的方式,但是它可以改变函数的this指向
	fun.apply(thisArg,[argsArray]);
	thisArg:在fun函数运行时指定的this值
	argsArray:传递的值,必须包含在数组里面 
	返回值就是函数的返回值,因为它就是调用函数
	
	bind方法
	bind()方法不会调用函数,但是能改变内部this指向
	fun.bind(thisArg,arg1,arg2,...)
	thisArg:在fun函数运行时指定的this值
	arg1,arg2:传递的其他参数
	返回由指定的this值和初始化参数改造的原函数拷贝
	var o={
		name:'andy'
	};
	function fn(){
		console.log(this);
	};
	fn.bind(o); //调用不执行
	var f=fn.bind(o); // f();
	//不会调用原来的函数,但是可以改变原来函数内部的this指向
	//返回的是原函数改变this会后产生的新函数
	//如果有的函数我们不需要立即调用但是又想改变函数内部的this指向此时用bind
	
	//按钮需求，当我们点击了之后,禁用这个按钮,3秒后再开启这个按钮
	
	定时器里面的函数的this指向的是window
	
	总结:不会调用函数,会产生一个新的函数
	
	call apply bind 总结
	相同点
	都可以改变函数的this指向
	区别点
	call和apply会调用函数并别改变函数内部this指向
	call和apply传递的参数不一样,call传递参数arg1,arg2...形式apply必须数组形式[arg]]
    bind不会调用函数,可以改变函数内部this指向
	
	应用场景
	1.call经常做继承
	2.apply经常跟数组有关系,比如借助于数学对象实现数组最大值和最小值
	3.bind不调用函数,但是还想改变this指向,比如改变定时器内部的this指向
	
 ```
 ### js中的严格模式
    - javascript除了提供正常的模式外,还提供了 `严格模式`,es5的严格模式是采用具有限制性 javascript辩题的一种方式,
	  即在严格的条件下运行代码ie10以上才支持
	- 消除了javascript语法的一些不合理,不严谨之处,
	  保留关键字:class,enum,export,extends,import,super不能做变量名  
 ``` js{1}
   开启严格模式
   严格模式可以应用到整个脚本或者个别函数中,因此在使用时,我们可以将严格模式氛围
   为脚本开启严格模式和为函数开启严格模式两种情况
   1.为脚本开启严格模式
   在所有语句放到严格特定语句'usestrict'或('use strict')
   为整个脚本script标签开启严格模式 ie10
   1.<script>
     'use strict';  
	 //下面的js代码就会按照严格模式来执行代码
   </script>
   
   2.<script>
   //立即执行函数
        (function(){
			'use strict';
		})
   </script>
   
   为函数单独开启严格模式
   <script>
    function fn(){
		'use struct'
		//下面代码按照严格模式执行
	}
	
	function  fun(){
		//这个函数还是按照普通函数执行
	}
   
   </script>
	
	严格模式中的变化
	严格模式对javascript的语法和行为,做了一些改变 如变量不声明直接赋值会报错
	
	严格模式下this指向问题
	1.以前在全局作用域中this指向的事window对象
	2.严格模式下全局作用域中的函数this指向是undefined.
	3.以前构造函数时不加new也可以调用,当普通函数,this指向全局对象
    4.严格模式下,如果构造函数不加new调用 ,this会报错
	5.new实例化的构造函数指向创建的对象实例
	6.定时器this还是指向window
	7.事件,对象还是指向调用者
	
	严格模式下函数
	1严格模式函数里面的参数不能有重名
	2.函数必须声明在顶层,新版本的javascript会引入'块级作用域'(es6中已引入),为了与新版本接轨
	不允许在非函数的代码块内声明函数
	更多详情可以查看MDN-
	
 ```
   
### 高阶函数
   `高阶函数`是对其他函数进行操作的函数,它`接收函数作为参数`或`将函数做位返回值输出`
   
### 闭包(Closure)
   `闭包`是指有权访问另一个函数作用域中变量的`函数` ----javascript高级程序设计
    简单的理解就是,一个作用域可以访问另外一个函数内部的局部变量

``` js{1}
  function fun(){
	  var num=10;
	  function fn(){
		  console.log(num);
	  }
	  fn();
  }
 fun();
  闭包的主要作用:延伸了变量作用范围
```
   
   
   
### 手写ajax

``` js{1}
   //实例化
   let xhr=new XMLHttpRequest()
   //初始化
   xhr.open(method,url,async)
   //发送请求
   xhr.send(data);
   //设置状态变化回调处理请求结果
   xhr.onreadystatechange=()=>{
	   if(xhr.readyStatus===4&&xhr.status===200){
		   console.log(xhr.responseText)
	   }
   }


 //Promise实现
 function ajax(options){
	 //请求地址
	 const url=options.url
	 //请求方法
	 const method=options.method,toLocaleLowerCase()||'get'
	 //设置默认为true
	 const async=options.async
	 //请求参数
	 const data=options.data
	 //实例化
	 const xhr=new XMLHttpRequest()
	 //请求超时
	 if(options.timeout&&options.timeout>0){
		 xhr.timeout=options.timeout
	 }
	 //返回Promise实例
	 return new Promise ((resolve,reject)=>{
		 xhr.ontimeout=()=>reject&&reject('请求超时')
		 //监听状态变化回调
		 xhr.onreadystatechange=()=>{
           if(xhr.readyState==4){
			 if(xhr.status>=200&&xhr.status<300||xhr.status==304){
				 resolve&&resolve(xhr.responseText)
			 }else{
				 reject&&reject()
			 }
		   }
		 }
		 //错误回调
		 xhr.onerror=err=>reject&&reject(err)
		 let paramArr=[]
		 let encodeData
		 //处理请求参数
		 if(data instanceof  Object){
			 for(let key in data){
				 //参数拼接需要通过encodeURLComponent 进行编码
				 paramArr.push(encodeURLComponent(key)+'='+encodeURLComponent(data[key]))
			 }
			 encodeData=paramArr.join('&')
		 }
		 //get请求参数拼接参数
		 if(method==='get'){
			 //检测URl是否存在 其位置
			 const index =url.indexOf('?')
			 iF(index===1)url+='?'
			 else if(index!==url.length-1)url+='&'
			 url+=endcodeData
		 }
		 //初始化
		 xhr.open(method,url,async)
		 //发送请求
		 if(method==='get')xhr.send(null)
		 else{
			 //post 请求头需要
			 xhr.setRequestHeader('Content-Type','application/x-www-form-urlencodede;charset=UTF-8')
			 xhr.send(encodeData)
		 }
	 })
 }
```