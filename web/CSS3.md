### css3



- transform 2D转换
- translateX  位移
- rotate   旋转


### translateX位移
``` js{2}
   利用transform:translate是实现居中
   .father{
       width:300px;
       height:300px;
       background-color:pink;
       position: relative;
         }

   .son {
       width:150px;
       height:150px;
       background-color:black;
       position: absolute;
       top:50%;
       left:50%;
       transform:translate(-50%,-50%);
   }      
```
- translate(X,Y)
- 定义2D转换中的移动,沿着X和Y轴移动元素
- translate不会影响其他元素的问题 translate对行内元素没用


### rotate旋转

### animation 动画
 
 
 ### 3D转换
 - translateZ正直往你眼前方向移动,向外为正值,向内是负值
 - transform:translate3d(x,y,z) xyz没有值的时候写0就好了
 - 3D转换Z轴必须要用`perspective`透视来看效果
 - 3D呈现`transform-style:preserve-3d`   给父元素加这个子元素显示3D效果