#  web第一天的总结

 :100:

 ### html第一天  超文本标记语言<br>
 -----


::: tip
 提示:<a 标签的target的属性值>
:::

`利用base标签 <base target="_blank">  整体设置 a标签链接的重新打开方式 在    head插入`

``` js{2}
   <a href="http://www.hao123.com" target="_blank">
   打开的方式target目标重新打开一个窗口  _self相对自己不用打开新窗口

   相对路径  /   ../上一级
   绝对路径  C:User\Desketop\e:

   文档类型 <!DOCTYPE html>
   字符集   UTF-8
```




``` js{2}
所有的Markdown文件都会被webpack编译成Vue组件，因此你可以，并且应该更倾向于使用相对路径（Relative URL）
来引用所有的静态资源：
![An image](../.vuepress/public/logo.jpeg)

```

 ###  锚点链接
 ---

 ``` js{2}
   <div id='one'></div>
   <a href="#one"></a>
 ```
 

[[toc]]


### table 表格
 **cellspacing="0"单元格与单元格(外边距);  :100: cellpadding="20"(内边距):100:  align="center" 三个表格属性**

 ```js{1}
    	<table border="1" width="500" height="200" align="center" cellspacing="0" cellpadding="20">
			<!-- 表格标签 -->
			<tr>      
			<!-- tr 行  一行-->
				<td>姓名</td>
				<!-- td 单元格   -->
				<td>年龄</td>
				<td>性别</td>
			</tr>

			<tr>      
				<td>yang</td>
				<td>26</td>
				<td>男</td>
			</tr>
			
			<tr>      
				<td>yang</td>
				<td>26</td>
				<td>男</td>
			</tr>
			
			<tr>      
				<td>yang</td>
				<td>26</td>
				<td>男</td>
			</tr>
		</table>


 跨行合并: rowspan  跨列合并:colspan
 ```

 :100:
 ### 列表 
```js{1}
  
   无序列表
   <ul>
   <li></li>
   </ul>
   
   有序列表
    <ol>
      <li></li>
    </ol>

    自定义列表
    <dl>
    <dt></dt>
    </dl>
    自定义列表一般用于页面底部介绍
```
 ### 三种元素 :100:

``` js{1}
  块级元素:独占一行
  行内元素:在一行上显示不能设置宽高
  行内块元素:既能一行上显示又能设置宽高
  
  块级元素
  <address>...</adderss>   
  <center>...</center> 地址文字
  <h1>...</h1>  标题一级
  <h2>...</h2>  标题二级
	<h3>...</h3>  标题三级
	<h4>...</h4>  标题四级
	<h5>...</h5>  标题五级
	<h6>...</h6>  标题六级
	<hr>  水平分割线
	<p>...</p>  段落
	<pre>...</pre>  预格式化
	<blockquote>...</blockquote>  段落缩进   前后5个字符
	<marquee>...</marquee>  滚动文本
	<ul>...</ul>  无序列表
	<ol>...</ol>  有序列表
	<dl>...</dl>  定义列表
	<table>...</table>  表格
	<form>...</form>  
	<div>...</div>
  行内元素
  <span>...</span>
	<a>...</a> 
	<br> 
	<b>...</b> 
	<strong>...</strong> 
	<strong>...</strong>
	<img>,
  <sup>...</sup> 
	<sub>...</sub> 
	<i>...</i> 
	<em>...</em> 
	<del>...</del>
	<u>...</u>
	<input>...</input>
  <textarea>...</textarea>
  <select>...</select>

行内块元素

display:block;
display:inline-block;
display:inline;

```
 
  - 两个盒子之间发生margin的问题:
  >两个盒子外边距  只给一个margin值

  - 父子盒子之前 子盒子设置margin 会发生塌陷  解决方法:
 
  1.给父元素一个border-top值
  2.给父元素一个padding-top
  3.或者overflow: hidden;


**这是加粗的文字**
*这是倾斜的文字*`
***这是斜体加粗的文字***
~~这是加删除线的文字~~

[简书](http://jianshu.com)
[百度](http://baidu.com)


`代码内容`

```js{3}
前端复习流程
1.div+css布局
2.javascript
3.h5c3
4.jquery
5.oop
6.ajax
7.git/gulp/node/npm/webpack/
8.es6
9.vue/recat/
10.面试题
```

---
 web的标准
---
![An image](../.vuepress/public/web.png) 

### css第一天


<!-- 代码高亮 -->
###  css几种居中方式
``` js{4}
水平垂直居中：
定位和需要定位的元素的margin减去宽高的一半
img{
            width: 100px;
            height: 150px;
            position: absolute;
            top: 50%;
            left: 50%;
            margin-top: -75px;
            margin-left: -50px;
   }

定位和margin:auto
img{
            width: 100px;
            height: 100px;
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            margin: auto;
   }

利用tranform实现的水平垂直居中 
img{
        width: 100px;
        height: 100px;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%,-50%);
   }  
   
  浮动,定位,可以解决外边距合并的问题 固定定位,绝对对位 
  以前是用padding,boreder,overflow:hidden 
   
```
 - 块级元素 margin:auto  
 - 行内元素 text-align:center
 - 行内元素,行内块 vertcal-align:middle;

###  css三种清除浮动

 -  1.给父元素overflow: hidden
 -  2.给伪元素清除浮动
 -  3.clear:both
``` js{2}
.clearfix:after {
content: ".";
display: block;
height: 0;
clear: both;
visibility: hidden
}

.clearfix {
    *zoom: 1   //兼容IE6
}

 ```


 ### css 新属性转载来自鑫空间❤
  - 2019-7-23   
  -  1.filter: blur(5px)滤镜模糊  //你只需要给图片一个这个属性 px越大越模糊
   -  2.filter: brightness(1.4)  //滤镜亮度
  -   3.filter: contrast(2);     //滤镜对比度
  -   4.filter: drop-shadow(4px 4px 8px blue);  //滤镜-投影
  -   5.filter: grayscale(50%);  //滤镜-灰度
  -   6.filter: hue-rotate(90deg);  //滤镜-色调变化


### Sass的使用
 
  安装sass
  .sass+.scss ==>css  
  嵌套规则
  变量规则
  条件逻辑
  
