### flex布局的介绍
👇 内容速览 👇 
## 父元素常见属性
- flex-direction：设置主轴的方向
- justify-content：设置主轴上的子元素排列方式
- flex-wrap：设置子元素是否换行  
- align-content：设置侧轴上的子元素的排列方式（多行）
- align-items：设置侧轴上的子元素排列方式（单行）
- flex-flow：复合属性，相当于同时设置了 flex-direction 和 flex-wrap

 - 当我们为父盒子设为flex布局以后，子元素的float clear,vertical-align讲失效
 
 
 ## flex-direction设置主轴的方向
- 在 flex 布局中，是分为主轴和侧轴两个方向，同样的叫法有 ： 行和列、x 轴和y 轴
- 默认主轴方向就是 x 轴方向，水平向右
- 默认侧轴方向就是 y 轴方向，水平向下

 ![An image](../.vuepress/public/flex/flex1.jpg)

 
-  注意： 主轴和侧轴是会变化的，就看 flex-direction 设置谁为主轴，剩下的就是侧轴。而我们的子元素是跟着主轴来排列的
  
  ![An image](../.vuepress/public/flex/flex2.jpg)
 

 ### justify-content 设置主轴上的子元素排列方式
![An image](../.vuepress/public/flex/flex3.jpg)
 
 ### flex-wrap设置是否换行 
- 默认情况下，项目都排在一条线（又称”轴线”）上。flex-wrap属性定义，flex布局中默认是不换行的。
- nowrap 不换行
- wrap 换行

 ### align-items 设置侧轴上的子元素排列方式（单行）
- 该属性是控制子项在侧轴（默认是y轴）上的排列方式  在子项为单项（单行）的时候使用
- flex-start 从头部开始
- flex-end 从尾部开始
- center 居中显示
- stretch 拉伸

### align-content  设置侧轴上的子元素的排列方式（多行） 
 `设置子项在侧轴上的排列方式 并且只能用于子项出现 换行 的情况（多行），在单行下是没有效果的。`
 ![An image](../.vuepress/public/flex/flex4.jpg)
 
### align-content 和align-items区别
- align-items  适用于单行情况下， 只有上对齐、下对齐、居中和 拉伸
- align-content适应于换行（多行）的情况下（单行情况下无效）， 可以设置 上对齐、下对齐、居中、拉伸以及平均分配剩余空间等属性值。 
- 总结就是单行找align-items  多行找 align-content

###  flex-flow 属性是 flex-direction 和 flex-wrap 属性的复合属性
   `flex-flow:row wrap;`


###  flex布局子项常见属性
- flex子项目占的份数
- align-self控制子项自己在侧轴的排列方式
- order属性定义子项的排列顺序（前后顺序）
 
###  flex 属性
 flex 属性定义子项目分配剩余空间，用flex来表示占多少份数

`
.item {
    flex: <number>; /* 默认值 0 */
}
`

###  align-self控制子项自己在侧轴上的排列方式 

 - align-self 属性允许单个项目有与其他项目不一样的对齐方式，可覆盖 align-items 属性。
 - 默认值为 auto，表示继承父元素的 align-items 属性，如果没有父元素，则等同于 stretch。

`
span:nth-child(2) {
      /* 设置自己在侧轴上的排列方式 */
      align-self: flex-end;
}
`
### order 属性定义项目的排列顺序  
 `数值越小，排列越靠前，默认为0。注意：和 z-index 不一样`
 
 `.item {order: <number>;}`
 
### flex居中
``` js{1}
  .father{
	  width:300px;
	  height:300px;
	  background-color:pink;
	  display:flex;
	  justify-content: center;  //主轴居中
	  align-items: center;  //侧轴居中
  }
  
  .son{
	  width:150px;
	  height:150px;
	  background-color:red;
  }


```