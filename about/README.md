 ## hello,读者!
  
####  关于这个vuepresss这个静态生成的bolg真是太好用了值得推荐,这里感谢飞跃高山和大海的鱼录制的视频 

 :100: ` 在这里很感谢每一个能够点进来看的小伙伴们,写这个个人博客的缘由也是为了督促自己能够好好学习分享自己的学习的一个介绍,
  虽然不是科班出生,可能不如科班里面的人对计算机那么强,可以自己也是非常喜欢电脑,家里老表是java也就是这个入了前端的门.
  但是我相信勤能补拙,干就完事了! `

---
 **更新模块更能介绍:**

 - 2017-07-21 添加语言翻译
 - 2019-07-22 添加学习链接
 - 2019-07-22 添加搜索 
 - 2019-07-23 添加自定义css样式,logo图背景 css3 filter阴影效果
 - 2019-8-03  更改bolg排版
 - 2019-08-05  关于css样式丢失问题有的解决了config.js文件中的base文件设置一下就好了

 - 2019-7-22 0:59 我更新一个nav下面的一个语言切换中英文的文件,这里很感谢`心谭博客作者`github源码 :gift_heart:  :gift_heart: 


### 内置搜索


- 你可以通过设置 `themeConfig.search: false` 来禁用默认的搜索框，或是通过 `themeConfig.searchMaxSuggestions` 来调整默认搜索框显示的搜索结果数量：

 ```js{1}
module.exports = {
    themeConfig: {
    search: false,   //为true 开启
    searchMaxSuggestions: 10  //显示十条
  }
}

 ``` 

:::tip
内置搜索只会为页面的标题、h2 和 h3 构建搜索索引，如果你需要全文搜索，你可以使用 Algolia 搜索。
 :::


### 语言切换

 - 注: `你需要在配置一个en的文件夹里面 弄个英文版的设置跳转到哪个页面就好了`


``` js{1,3}
locales: {
    // 编译是否中文英文
  '/': {
      lang: 'zh-CN',
      title: '洪利的博客',
      description: 'HongLi的个人bolg,基于vuepress的一个静态框架去写的,前端菜鸟的日常'
      },
  '/en/': {
    lang: 'en-US',
    title: 'HongLi Blog',
    description: 'HongLi s personal bolg, based on a static framework of vuepress, the front of the rookie.'
 }
},
```

 上次看掘金看到一个写的非常好的css

### 工作中培养起来的几点认知

- 1.优先做最重要的事情，（可以自己写在笔记本上，每天的任务，也可以利用todolist类似的软件）
-  2.懂得“闭环思维”，（对领导定期汇报项目进展，对同事、下属及时同步项目进度）
-  3.拥有解决问题并快速解决问题的能力（解决各种问题，锻炼解决问题的思维，一条路不通要想别的方法）
-  4.做一个靠谱、聪明、皮实、值得信赖的人。提高自己的不可替代性。
-  5.凡事有交代，件件有着落，事事有回音。
-  6.感激bug,是bug让自己成长，要成长必须多解决bug.多承担任务。
-  7.积极乐观，做一个正能量的人。（远离负能量的人和事）


### git 上传github ssh权限问题

``` js{3}
fatal: Could not read from remote repository.

Please make sure you have the correct access rights
and the repository exists.
 //翻译出来是: 致命：无法从远程存储库读取。
请确保您具有正确的访问权限并且存储库存在。

1.查看本地是否有已经生成好的ssh key
  命令:cat ~/.ssh/id_rsa.pub
  若没有，会提示no such file or directory
  若有，则先删除：执行命令
      cd ~
      rm -rf .ssh
	  
2. 重新生成ssh key
	  命令：ssh-keygen -t rsa -C  "邮箱"
	  
3. 会提示输入密码以及确认密码
	  密码全部回车即可

4.查看新生成的 ssh key
	  命令:cat ~/.ssh/id_rsa.pub
	  
5.粘贴后在git 的 settings–>ssh keys 中粘贴并确定
	  
	 或者你也可以设置项目的里面的密匙 

6.生成完了过后后面你上传会发现出现这个问题
The authenticity of host 'github.com (192.30.255.112)' can't be established.
RSA key fingerprint is SHA256:nThbg6kXUpJWGl7E1IGOCspRomTxdCARLviKw6E5SY8.

其实是少了一个known_hosts文件，本来密钥文件应该是三个，现在是两个，便报了这样的错误，此时选择yes回车之后，便可，同时生成了缺少了的known_hosts文件

Are you sure you want to continue connecting (yes/no)? //输入yes，回车

```

### node版本问题 就是10版本 npm版本过低下载依赖会提醒你不匹配的问题

自动编译 bash deploy.sh


### 关于webpack丢失问题我也不知道啥电脑老是报webpack

<!-- ``` js{1}
npm ERR! code ELIFECYCLE
npm ERR! errno 1
npm ERR! docs@1.0.0 docs:build: `vuepress build docs`
npm ERR! Exit status 1
npm ERR!
npm ERR! Failed at the docs@1.0.0 docs:build script.
npm ERR! This is probably not a problem with npm. There is likely additional logging output above.

npm ERR! A complete log of this run can be found in:
npm ERR!     C:\Users\zhang\AppData\Roaming\npm-cache\_logs\2019-07-25T02_54_31_450Z-debug.log

//会报这种错误，我记得自己是重新下载一个webpack可以打包了。这种问题默认打包dist文件为空 vuepress build docs
 
打包构建报错
Error: Cannot find module 'webpack'
    at Function.Module._resolveFilename (module.js:536:15)
    at Function.Module._load (module.js:466:25)
    at Module.require (module.js:579:17)
    at require (internal/module.js:11:18)
    at Object.<anonymous> (C:\Users\zhang\Desktop\vuepress\bolg\node_modules\stylus-loader\index.js:183:27)
    at Module._compile (module.js:635:30)
    at Object.Module._extensions..js (module.js:646:10)
    at Module.load (module.js:554:32)
    at tryModuleLoad (module.js:497:12)
    at Function.Module._load (module.js:489:3)
    at Module.require (module.js:579:17)
    at require (internal/module.js:11:18)
    at loadLoader (C:\Users\zhang\AppData\Roaming\npm\node_modules\vuepress\node_modules\_loader-runner@2.4.0@loader-runner\lib\loadLoader.js:18:17)
    at iteratePitchingLoaders (C:\Users\zhang\AppData\Roaming\npm\node_modules\vuepress\node_modules\_loader-runner@2.4.0@loader-runner\lib\LoaderRunner.js:169:2)

``` -->


 ![An image](../.vuepress/public/del.png)
  
  
###  webpack 版本4.0打包css需要配置
```js{1}
- webapck指令(`玩转webpack`)
- npm  install  webpack  -g  全局安装,建议本地项目安装 
- npm  install  webpack@<version>   -g  安装指定版本
- npm  uninstall  webpack  -g 全局删除webpack
- npm  install  webpack@3.0  --save-dev 下载指定版本在本地项目中
- npm i webpack --save            保存安装包信息到package.json中
- npm init -y 初始化文件   yarn  init 也可以
```


### webpack学习第一天 

```js{1}
  - yarn add -D webpack webpack-cli   -D开发模式  或者npm
  
  - 新建一个src文件里面index.js   package.json   script :build 打包  自动打包
  
  自己配置webpack  你需要在根目录下创建一个webpack.config.js这个文件
  //config.js 
  
   const HtmWebpackPlugin=require=('html-webpack-plugin')//引入插件
  module.exports={
	  //导出
	  entry:'./src/index.js' //入口文件  你需要安装一下插件 npm install --save-dev html-webpack-plugin
	  plugin:[
		  new HtmWebpackPlugin();
	  ]
  }
  webpack 四个核心模块你需要了解
  - 入口(entry)
    >什么是单一入口、
	>多个入口文件怎么办
  
  - 输出(output)
  
  - loader
  
  - 插件(plugins)
  
  - mode模块
```

 ### 手动配置webpack Entry 入口文件
``` js{1}
- 入口(entry)
    首先你得创建一个webpack.config.js
	//导出模块口
	//单一的入口文件配置
	module.exports={
		entry:'./src/index.js', //入口 就是你需要编译的文件
		mode:'development'   //(模式)
		//你也可以在package.json  script:'webpack --mode development' 里面配置
	}
	
	-----
	//多入口文件配置
	module.exports={
		entry:{
			register:'./src/register.js',
			home:'./src/home.js',
			detail:'./src/detail.js'
		}, //入口 就是你需要编译的文件
		
		mode:'development'   //(模式)
		//你也可以在package.json  script:'webpack --mode development' 里面配置
	}
 ```
 
 ### webpack输出定义Output
 ```js{1}
   - 输出(output)
module.exports={   //导出
 	 entry:'./src/index.js',  // 入口
 	mode:'development',  //模式
	output:{ //输出
		filename:'bundel.js'
	}
	
 }
 ```
 
 ###  webpack里面使用Loaders
 >> - oader 用于对模块的源代码进行转换。loader 可以使你在 import 或"加载"模块时预处理文件。因此，loader 类似于其他构建工具中“任务(task)”,
  并提供了处理前端构建步骤的强大方法。loader 可以将文件从不同的语言（如 TypeScript）转换为 JavaScript，或将内联图像转换为 data URL。loader 甚至允许你直接在 JavaScript 模块中 import CSS文件！
``` js{1}
   webpack.config.js
  const HtmlWebpackPlugin=require('html-webpack-plugin');
  module.exports={
	  entry:{
		  index:"./src/index.js"
	  },
	  mode:'development', //模式
	  module:{
		  rules[ //规则
			  {
				  test:/.css$/,
				  use:['style-loader','css-loader']//你得下载这两个模块
				  }, //正则
				  {
				  	test:/.less$/,
				  	use:['style-loader','css-loader','less-loader']//你还需要下载less
				   } 
		  ]
	  },
	  plugins:[
		  new HtmlWebpackPlugin({
			  template:'./src/index.html',
		  }); //引入插件 html代码会被导入过来
	  ]
  }
```

 ###插件 Plugins
 
  - npm init -y 初始化项目
  - 安装webpack 
  - 创建一个webpack.config.js

 ### 如何用webpack转换TypeScript

```js{1}
   - webapack.config.js
  module.exports={
	  mode:'development',
	  entry:'./src/index.ts',
	  module:{
		  rules:[
			  {
				  test:/\.ts/,
				  loader:'ts-loader',
			  }
		  ]
	  }
  }
  yarn add -D ts-loader //模块
  yarn add -D TyScript  
  你还需要创建一个tsconfig.json文件配置
  {
	  "compilerOptions":{
		  "sourceMap":true,
	  }
  }  
```
###  webpack手动配置热更新
``` js{1}
  npm init -y   package.json  script:{"build":webpack}
  配置webpack.config.js
  const path=require('path')
  const HtmlWebpackPlugin=require('html-webpack-plugin')
  const Webpack=require('webpack')
  module.exports={
	  entry:'./src/index.js',
	  mode:'development',
	  devServer:{
		  contenBase:"./dist",
		  hot:true,
	  },
	  plugins:[
		  new webpack.HotModuleReplacementPlugin();
		  new HmtlWebPackPlugin({
			  title:'Hot Module  Replacement'
		  })
	  ],
  }
  ------
  yarn build  //构建
  你还需要在package.json 中的
  script:{
	  "build":"webpack",
	  "dev":"webpack-dev-server"
	  }
  创建在node下面 在src文件夹创建一个dev-server.js
  const webpackDevServer=require('webpack-dev-server')
  
  const webpack=require('webpack')
  
  const config=require('./webpack.config.js')
  
  const option={
	  hot:true,
	  host:'localhost'
  }
  webpackDevServer.addDevServerEntrypoints(config,option)
  
  const compiler=webpack(config)
  
  const server=new  webpackDevServer(compiler,option)
  
  server.listen(5000,'localhost',()=>{
	  console.log('dev server listening on  point 5000')
  })
  //启动   webpack-dev-server
  
  
  yarn add webpack-dev-server
```

### webpack 如何同时支持开发和发布环境
   - npm init -y
```js{1}   
  创建webpack.dev.config.js   webpack.prod.config.js两个文件夹
	
	package.json  
	script:{
		"webpack: --config  webpack.dev.config.js"
		"build":webpack  --config  webpack.prod.config.js""
		}
		
    安装依赖
	yarn  add -D  webpack  webpack-cli
	yarn  add -D  html-webpack-plugin
	yarn  add -D  css-loader  style-loader  less-loader
	yarn  add -D file-loader   //图片
	yarn  add -D  clean-webpack-plugin
	yarn add -D  extract-text-webapck-plugin@4.0    //如果你的webpack是4.0这个插件兼容问题可能也要升级
	
	webpack.dev.config.js 配置开发环境
	const path=require('path');
	const CleanWebPackPlugin=require('clean-webpack-plugin')
	const  HtmlWebPackPlugin=require('html-webpack-plugin')
	const ExtractTextWebPackPlugin=require('extract-text-webapck-plugin') 
	
	const  dist='dist'
	
	module.export={
		mode:'production',
		entry:{
			index:'./src/index.js'
		},
		output:{ //输出
			path:path.resolve(__dirname,'dist') //输出到指定目录
		}
		moudule:{
			rules:[
				{
					test:/\.css$/, //css正则
					use:ExtractTextWebPackPlugin.extract({
						fallback:'style-loader',
						use:['css-loader']
					})
				},
				{
					test:/\.less$/, //css正则
					use:ExtractTextWebPackPlugin.extract({
						fallback:'style-loader',
						use:['css-loader','less-loader']
				},
				{
					test:/\.(jpg|png|svg|jepg)/, 
					use:['file.loader']//处理过程最先处理的事less在处理前面的
				},
				
			]
		},
		plugin:[
			new CleanWebPackPlugin(dist), //清理
			new HtmlWebPackPlugin({
				template:'./src/index.html', //你需要生成文件
				title:'this is mode'
			}),
			new ExtractTextWebPackPlugin('style.css') //导出
		]
		yarn  dev //执行
	}
```	
 ### 配置公共部分文件 webapck.base.config.js
``` js{1}
    const path=require('path');
	const CleanWebPackPlugin=require('clean-webpack-plugin')
	const  HtmlWebPackPlugin=require('html-webpack-plugin')
	const ExtractTextWebPackPlugin=require('extract-text-webapck-plugin') 
	
	
	
	module.export={
		entry:{
			index:'./src/index.js'
		},  //删除输出
		moudule:{
			rules:[
				{
					test:/\.css$/, //css正则
					use:ExtractTextWebPackPlugin.extract({
						fallback:'style-loader',
						use:['css-loader']
					})
				},
				{
					test:/\.less$/, //css正则
					use:ExtractTextWebPackPlugin.extract({
						fallback:'style-loader',
						use:['css-loader','less-loader']
				},
				{
					test:/\.(jpg|png|svg|jepg)/, 
					use:['file.loader']//处理过程最先处理的事less在处理前面的
				},
				
			]
		},
		plugin:[
			new HtmlWebPackPlugin({
				template:'./src/index.html', //你需要生成文件
				title:'this is mode'
			}),
			new ExtractTextWebPackPlugin('style.css') //导出
		]
		yarn  dev //执行
	}
```

- 合并webpack.dev.config.js  webpack.prod.config.js
 - yarn add -D webpack-merge
 
 - 在webpack.dev.config.js中
 ``` js{1}
 const path=require('path');
 const CleanWebPackPlugin=require('clean-webpack-plugin')
 const ExtractTextWebPackPlugin=require('extract-text-webapck-plugin') 
 const WebPackMerge=require('webpack-merge')
 const baseConfig= require('./webpack.base.config')
 
 module.exports=WebPackMerge(baseConfig,{
	 mode:'development',
	 output:{ //输出
	 	path:path.resolve(__dirname,'dist') //输出到指定目录
	 },
	 	plugin:[
	 		new CleanWebPackPlugin(dist), //清理
	 		new HtmlWebPackPlugin({
	 			template:'./src/index.html', //你需要生成文件
	 			title:'Dev  mode'
	 	
			}),
	 	]
		yarn dev
		
 })
 ``` 
 
  - 在webpack.prod.config.js
  ```js{}
  const path=require('path');
  const CleanWebPackPlugin=require('clean-webpack-plugin')
  const ExtractTextWebPackPlugin=require('extract-text-webapck-plugin') 
  const WebPackMerge=require('webpack-merge')
  const baseConfig= require('./webpack.base.config')
  
  const dist='dist'
  module.exports=WebPackMerge(baseConfig,{
  	 mode:'product',
  	 output:{ //输出
  	 	path:path.resolve(__dirname,'dist') //输出到指定目录
  	 },
  	 	plugin:[
  	 		new CleanWebPackPlugin(dist), //清理
  	 		new HtmlWebPackPlugin({
  	 			template:'./src/index.html', //你需要生成文件
  	 			title:'APP prod'
  	 	
  			}),
  	 	]
		yarn build
  })
  ```
  
### webpack配置一个vue.js项目
   - 找一个项目pro
   - yarn init -y  package.json
   - yarn add  -D  webpack webpack-cli
   - 创建webpack.config.js
   - yarn add vue
   - 创建index.js  

``` js{1}
  console.log('hello world')
  
```

 ```js{1} 
  -  yarn add -D html-wepack-plugin
     webpack.config.js
  const  HtmlWebPackPlugin=require('html-webpack-plugin')
  const  VueLoaderPlugin=require('vue-loader/lib/plugin') //因为webpack4.0vueloader你需要放到lib目录下面
   module.exprots={ 
	   mode:'development', //开发模式
	   entry:'./src/index.js',
	   module:{
		   rules:[
			   {
				   test:/\.vue$/,
				   loader:'vue-loader'
			   },
			   {
			   				   test:/\.js$/,
			   				   loader:'babel-loader'
			   },
			   {
			   				   test:/\.css$/,
			   				   use:[
								   'vue-style-loader',
								   'css-loader'
							   ]
			   },
			    {
			   				   test:/\.less$/,
			   				   use:[
			   								   'vue-style-loader',
			   								   'css-loader',
											   'less-loader'
			   							   ]
			   }
		   ]
	   }
	   plugin:[
		   new  VueLoaderPlugin(),
		   new  HtmlWebPackPlugin({
			   template:'./src/index.html'
		   }),
	   ]
   }
   创建 index.html
   <div id="app"></app>
  
  yarn add -D webpack-dev-server  //热更新
  
  package.json
  "script":{
	  "dev":"webpack-dev-server --config  webapck.config.js",
	  "build":"webpack  --config webpack.config.js"
  } 
  
  yarn dev
  
  编写vue代码
 创建 index.html
 <div id="app"></app>
  
  index.js
  
  import Vue from 'vue';
  import App from  './App.vue'; //引入vue
  
  new Vue({
	  el:'#app',
	  render:(h)=>h(App);
  })
  
  创建一个App.vue
 <template>
  <h1>hello wrold !</div>
 </template>
 
 <script>
    export default{
		
	}
 </scipt>
 
 <style >
 h1{ color:red}
</style>
 
 配置vue-loader
 yarn add -D vue-loader
 yarn add -D babel-loader  
 yarn add -D @babel/core
 yarn add  -D css-loader
 yarn add  -D vue-template-compiler
 yarn add  -D less less-loader
 
 yarn dev //启动
 yarn build //打包
 大晚上看视频up主把react-jsx语法用到了vue上面说为啥报错了className xx  class
 ``` 
 
 
 
### git大全 

``` js{3}
git的常规操作总结
Workspace：工作区
Index / Stage：暂存区
Repository：仓库区（或本地仓库）
Remote：远程仓库


一、新建代码库
# 在当前目录新建一个git代码库
$ git init

# 新建一个目录，将其初始化为git代码库
$ git init [project-name]

# 下载一个项目和它的整个代码历史
$ git clone [url]


二、配置

# 显示当前的Git配置
$ git config --list

# 编辑Git配置文件
$ git config -e [--global]

# 设置提交代码时的用户信息
$ git config [--global] user.name "[name]"
$ git config [--global] user.email "[email address]"


三、增加/删除文件

# 添加指定文件到暂存区
$ git add [file1] [file2] ...

# 添加指定目录到暂存区，包括子目录
$ git add [dir]

# 添加当前目录的所有文件到暂存区
$ git add .

# 添加每个变化前，都会要求确认
# 对于同一个文件的多处变化，可以实现分次提交
$ git add -p

# 删除工作区文件，并且将这次删除放入暂存区
$ git rm [file1] [file2] ...

# 停止追踪指定文件，但该文件会保留在工作区
$ git rm --cached [file]

# 改名文件，并且将这个改名放入暂存区
$ git mv [file-original] [file-renamed]

四、代码提交

# 提交暂存区到仓库区
$ git commit -m [message]

# 提交暂存区的指定文件到仓库区
$ git commit [file1] [file2] ... -m [message]

# 提交工作区自上次commit之后的变化，直接到仓库区
$ git commit -a

# 提交时显示所有diff信息
$ git commit -v

# 使用一次新的commit，替代上一次提交
# 如果代码没有任何新变化，则用来改写上一次commit的提交信息
$ git commit --amend -m [message]

# 重做上一次commit，并包括指定文件的新变化
$ git commit --amend [file1] [file2] ...

五、分支
# 列出所有本地分支
$ git branch

# 列出所有远程分支
$ git branch -r

# 列出所有本地分支和远程分支
$ git branch -a

# 新建一个分支，但依然停留在当前分支
$ git branch [branch-name]

# 新建一个分支，并切换到该分支
$ git checkout -b [branch]

# 新建一个分支，指向指定commit
$ git branch [branch] [commit]

# 新建一个分支，与指定的远程分支建立追踪关系
$ git branch --track [branch] [remote-branch]

# 切换到指定分支，并更新工作区
$ git checkout [branch-name]

# 切换到上一个分支
$ git checkout -

# 建立追踪关系，在现有分支与指定的远程分支之间
$ git branch --set-upstream [branch] [remote-branch]

# 合并指定分支到当前分支
$ git merge [branch]

# 选择一个commit，合并进当前分支
$ git cherry-pick [commit]

# 删除分支
$ git branch -d [branch-name]

# 删除远程分支
$ git push origin --delete [branch-name]
$ git branch -dr [remote/branch]

六、标签


# 列出所有tag
$ git tag

# 新建一个tag在当前commit
$ git tag [tag]

# 新建一个tag在指定commit
$ git tag [tag] [commit]

# 删除本地tag
$ git tag -d [tag]

# 删除远程tag
$ git push origin :refs/tags/[tagName]

# 查看tag信息
$ git show [tag]

# 提交指定tag
$ git push [remote] [tag]

# 提交所有tag
$ git push [remote] --tags

# 新建一个分支，指向某个tag
$ git checkout -b [branch] [tag]

七、查看信息

# 显示有变更的文件
$ git status

# 显示当前分支的版本历史
$ git log

# 显示commit历史，以及每次commit发生变更的文件
$ git log --stat

# 搜索提交历史，根据关键词
$ git log -S [keyword]

# 显示某个commit之后的所有变动，每个commit占据一行
$ git log [tag] HEAD --pretty=format:%s

# 显示某个commit之后的所有变动，其"提交说明"必须符合搜索条件
$ git log [tag] HEAD --grep feature

# 显示某个文件的版本历史，包括文件改名
$ git log --follow [file]
$ git whatchanged [file]

# 显示指定文件相关的每一次diff
$ git log -p [file]

# 显示过去5次提交
$ git log -5 --pretty --oneline

# 显示所有提交过的用户，按提交次数排序
$ git shortlog -sn

# 显示指定文件是什么人在什么时间修改过
$ git blame [file]

# 显示暂存区和工作区的差异
$ git diff

# 显示暂存区和上一个commit的差异
$ git diff --cached [file]

# 显示工作区与当前分支最新commit之间的差异
$ git diff HEAD

# 显示两次提交之间的差异
$ git diff [first-branch]...[second-branch]

# 显示今天你写了多少行代码
$ git diff --shortstat "@{0 day ago}"

# 显示某次提交的元数据和内容变化
$ git show [commit]

# 显示某次提交发生变化的文件
$ git show --name-only [commit]

# 显示某次提交时，某个文件的内容
$ git show [commit]:[filename]

# 显示当前分支的最近几次提交
$ git reflog

八、远程同步

# 下载远程仓库的所有变动
$ git fetch [remote]

# 显示所有远程仓库
$ git remote -v

# 显示某个远程仓库的信息
$ git remote show [remote]

# 增加一个新的远程仓库，并命名
$ git remote add [shortname] [url]

# 取回远程仓库的变化，并与本地分支合并
$ git pull [remote] [branch]

# 上传本地指定分支到远程仓库
$ git push [remote] [branch]

# 强行推送当前分支到远程仓库，即使有冲突
$ git push [remote] --force

# 推送所有分支到远程仓库
$ git push [remote] --all


九、撤销

# 恢复暂存区的指定文件到工作区
$ git checkout [file]

# 恢复某个commit的指定文件到暂存区和工作区
$ git checkout [commit] [file]

# 恢复暂存区的所有文件到工作区
$ git checkout .

# 重置暂存区的指定文件，与上一次commit保持一致，但工作区不变
$ git reset [file]

# 重置暂存区与工作区，与上一次commit保持一致
$ git reset --hard

# 重置当前分支的指针为指定commit，同时重置暂存区，但工作区不变
$ git reset [commit]

# 重置当前分支的HEAD为指定commit，同时重置暂存区和工作区，与指定commit一致
$ git reset --hard [commit]

# 重置当前HEAD为指定commit，但保持暂存区和工作区不变
$ git reset --keep [commit]

# 新建一个commit，用来撤销指定commit
# 后者的所有变化都将被前者抵消，并且应用到当前分支
$ git revert [commit]

# 暂时将未提交的变化移除，稍后再移入
$ git stash
$ git stash pop

十、其他


# 生成一个可供发布的压缩包
$ git archive

```

### git上传到仓库
  - git init 
    //初始化项目会生成一个.git文件默认是隐藏的你需要设置文件查看
  - git status 
    //状态 
 -- git add .
   //添加到git文件中,你可以查看一下状态git status
  - git commit -m "init my vuepressbolg"
   //初始化
  - git remote add origin https://gitee.com/mayunaiwo/HongLIbolg.git
  //仓库地址
  
  -  git push -u origin master
   //上传到我的分支
 

`最后更新与`