---
home: true
heroImage: /logopic.gif
actionText: Hongli bolg →
actionLink: /about/
features:
- title: HTML
  details: 熟练html语义化标签,html5新特性,擅长页面布局以及浏览器兼容性IE10.
- title: CSS
  details: 熟练CSS3新特性,Bootstrap,ElmentUl,Canvas.
- title: JS
  details: 原生JS就不必过多去说了,学的太渣了在努力补习中.
footer: MIT Licensed | Copyright © 2019-present Hong Li
---
:tada: :100:

## 🆕 🛠️制作中...

- 评论系统
- 组件,动画
- 2019-7-24 webpack模块丢失打包不了.
- 2019-7-23 更新了一个node10版本npm不支持10以上又退回8.9版本的node.
- 2019-7-28 关于webpack打包css丢失问题.


## 🎨 UI 设计
- css属性

## 🔧 工具
- 编辑器
- webpack  node git


## 😃 协作 && 联系
👇 内容速览 👇

